import Vue from 'vue'
import Element from 'element-ui'
//import 'element-ui/lib/theme-chalk/index.css'
import '../styles/index.css'
import locale from 'element-ui/lib/locale/lang/pl'

Vue.use(Element, { locale })
